// @ts-check

import { createStore, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import rootReducer from "./reducers";

const getComposeEnhancers = () => {
  const checkReduxDevTools =
    // @ts-ignore
    typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
  if (checkReduxDevTools) {
    // @ts-ignore
    return window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({});
  } else {
    return compose;
  }
};

const enhancer = getComposeEnhancers()(applyMiddleware(thunk));

export default createStore(rootReducer, enhancer);
