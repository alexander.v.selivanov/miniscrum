// @ts-check

import { HIDE_ERROR } from "../actionTypes";

const hideError = index => ({
  type: HIDE_ERROR,
  payload: index
});

export default hideError;
