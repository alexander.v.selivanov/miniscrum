// @ts-check

import { DELETE_TASK, ERROR_DELETE_TASKS } from "../actionTypes";

const deleteTask = id => {
  return function(dispatch) {
    return fetch(`/task/delete`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify({
        id
      })
    })
      .then(res => res.json())
      .then(res => {
        dispatch({
          type: DELETE_TASK,
          payload: id
        });
      })
      .catch(err => {
        dispatch({
          type: ERROR_DELETE_TASKS,
          payload: err.message
        });
      });
  };
};

export default deleteTask;
