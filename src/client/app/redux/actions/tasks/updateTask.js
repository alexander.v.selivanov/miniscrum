// @ts-check

import { UPDATE_TASK, ERROR_UPDATE_TASKS } from "../actionTypes";

const updateTask = (id, name, sprint, story, description) => {
  return function(dispatch) {
    name = name.trim();
    sprint = sprint.trim().toLowerCase();
    story = story.trim();
    return fetch("/task/update", {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify({
        id,
        name,
        sprint,
        story,
        description
      })
    })
      .then(res => res.json())
      .then(res => {
        dispatch({
          type: UPDATE_TASK,
          payload: {
            _id: id,
            name,
            sprint,
            story,
            description
          }
        });
      })
      .catch(err => {
        dispatch({
          type: ERROR_UPDATE_TASKS,
          payload: err.message
        });
      });
  };
};

export default updateTask;
