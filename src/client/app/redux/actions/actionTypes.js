// @ts-check

// task actions
export const LOAD_TASKS = "LOAD_TASKS";
export const NEW_TASK = "NEW_TASK";
export const EDIT_TASK = "EDIT_TASK";
export const CANCEL_EDIT_TASK = "CANCEL_EDIT_TASK";
export const ADD_TASK = "ADD_TASK";
export const UPDATE_TASK = "UPDATE_TASK";
export const ARCHIVE_TASK = "ARCHIVE_TASK";
export const DELETE_TASK = "DELETE_TASK";
export const UPDATE_TASK_SEARCH = "UPDATE_TASK_SEARCH";

// task errors
export const ERROR_LOAD_TASKS = "ERROR_LOAD_TASKS";
export const ERROR_ADD_TASKS = "ERROR_ADD_TASKS";
export const ERROR_UPDATE_TASKS = "ERROR_UPDATE_TASKS";
export const ERROR_DELETE_TASKS = "ERROR_DELETE_TASKS";
export const HIDE_ERROR = "HIDE_ERROR";

// sprint actions
export const SELECT_SPRINT = "SELECT_SPRINT";
