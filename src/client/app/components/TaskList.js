// @ts-check

import React, { Component } from "react";
import PropTypes from "prop-types";
import List from "@material-ui/core/List";
import ListSubheader from "@material-ui/core/ListSubheader";
import Typography from "@material-ui/core/Typography";

import Task from "./Task";

class TaskList extends Component {
  organizeTasksByStories(tasks) {
    const taskByStoryMap = new Map();
    taskByStoryMap.set("No story", new Array());
    tasks.forEach((task, index) => {
      const story = task.story || "No story";
      if (taskByStoryMap.has(story)) {
        taskByStoryMap.get(story).push(task);
      } else {
        taskByStoryMap.set(story, Array.of(task));
      }
    });
    return taskByStoryMap;
  }

  render() {
    const { tasks } = this.props;
    if (!tasks || !tasks.length) {
      return (
        <Typography variant="headline" component="p">
          No tasks
        </Typography>
      );
    }

    const taskByStoryMap = this.organizeTasksByStories(tasks);
    const stories = Array.from(taskByStoryMap.keys()).sort();

    const storyRender = story => {
      const storyTasks = taskByStoryMap.get(story);
      if (storyTasks.length) {
        return (
          <List
            key={story}
            component="div"
            subheader={
              <ListSubheader
                component="h3"
                color="primary"
                disableSticky={true}
              >
                {story}
              </ListSubheader>
            }
          >
            {storyTasks.map((task, index) => {
              return <Task task={task} key={task._id} />;
            })}
          </List>
        );
      }
    };

    return stories.map((story, index) => storyRender(story));
  }
}

TaskList.propTypes = {
  tasks: PropTypes.array.isRequired
};

export default TaskList;
