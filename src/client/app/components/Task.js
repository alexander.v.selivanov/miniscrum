// @ts-check

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";

import { BACKLOG, ARCHIVE } from "../../const";
import editTask from "../redux/actions/tasks/editTask";
import updateTask from "../redux/actions/tasks/updateTask";
import archiveTask from "../redux/actions/tasks/archiveTask";

class Task extends Component {
  handleAddToSprint = event => {
    const id = this.props.task._id;
    const { name, story, description } = this.props.task;
    const selectedSprint = this.props.selectedSprint;
    this.props.updateTask(id, name, selectedSprint, story, description);
  };

  handleArchive = event => {
    const id = this.props.task._id;
    this.props.archiveTask(id);
  };

  handleTaskClick = event => {
    this.props.editTask(this.props.task);
  };

  render() {
    const { task, searchTask } = this.props;
    let { name, description } = task;
    const isNotArchive = task.sprint !== ARCHIVE;
    const isBacklog = task.sprint === BACKLOG;

    function getHighlightedText(text, higlight) {
      let parts = text.split(new RegExp(`(${higlight})`, "gi"));
      return (
        <span>
          {parts.map((part, i) => (
            <span
              key={i}
              style={
                part.toLowerCase() === higlight.toLowerCase()
                  ? { fontWeight: "bold" }
                  : {}
              }
            >
              {part}
            </span>
          ))}
        </span>
      );
    }

    if (searchTask) {
      name = getHighlightedText(name, searchTask);
      description = getHighlightedText(description, searchTask);
    }
    return (
      <ListItem button onClick={this.handleTaskClick} className="content__task">
        <ListItemText primary={name} secondary={description} />
        <ListItemSecondaryAction>
          {isBacklog ? (
            <Tooltip title="Add to sprint">
              <IconButton
                aria-label="Add to sprint"
                color="primary"
                onClick={this.handleAddToSprint}
              >
                <AddIcon fontSize="small" />
              </IconButton>
            </Tooltip>
          ) : null}

          {isNotArchive ? (
            <Tooltip title="Archive">
              <IconButton
                aria-label="Archive"
                color="secondary"
                onClick={this.handleArchive}
              >
                <DeleteIcon fontSize="small" />
              </IconButton>
            </Tooltip>
          ) : null}
        </ListItemSecondaryAction>
      </ListItem>
    );
  }
}

Task.propTypes = {
  selectedSprint: PropTypes.string,
  searchTask: PropTypes.string,
  task: PropTypes.object.isRequired
};

const mapStateToProps = (state, props) => {
  return {
    selectedSprint: state.sprints.selected,
    searchTask: state.tasks.search
  };
};

const mapDispatchToProps = {
  editTask,
  updateTask,
  archiveTask
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Task);
