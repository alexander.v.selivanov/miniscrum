// @ts-check

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

import TaskList from "./TaskList";
import selectSprint from "../redux/actions/sprints/selectSprint";

class Sprint extends Component {
  handleSelectSprint = (event, value) => {
    this.props.selectSprint(value);
  };

  render() {
    const { selectedSprint } = this.props;
    if (!selectedSprint) {
      return null;
    }

    const searchTask = this.props.searchTask;
    const tasks = searchTask
      ? this.props.tasks.filter(
          task =>
            task.name.includes(searchTask) ||
            task.description.includes(searchTask)
        )
      : this.props.tasks;

    const sprints = Array.from(
      new Set(
        tasks
          .map(task => task.sprint)
          .filter(sprint => sprint !== "")
          .sort()
      )
    );
    const sprintTasks = tasks.filter(task => task.sprint === selectedSprint);
    return (
      <article id="content__sprint">
        <Typography variant="headline" component="h2">
          Sprint
        </Typography>

        <Tabs
          value={selectedSprint}
          scrollable
          scrollButtons="auto"
          onChange={this.handleSelectSprint}
        >
          {sprints.map(sprint => {
            return <Tab label={sprint} key={sprint} value={sprint} />;
          })}
        </Tabs>

        <TaskList tasks={sprintTasks} />
      </article>
    );
  }
}

Sprint.propTypes = {
  selectedSprint: PropTypes.string,
  tasks: PropTypes.array,
  searchTask: PropTypes.string
};

const mapStateToProps = (state, props) => {
  return {
    selectedSprint: state.sprints.selected,
    tasks: props.tasks,
    searchTask: state.tasks.search
  };
};

const mapDispatchToProps = {
  selectSprint
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sprint);
