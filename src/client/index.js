// @ts-check

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import App from "./app/App";
import store from "./app/redux/store";
import loadTasks from "./app/redux/actions/tasks/loadTasks";

store.dispatch(loadTasks());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
