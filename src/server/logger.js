const { createLogger, format, transports } = require("winston");
const path = require("path");

const { NODE_ENV, LOGGER_PATH } = require("./config");

const logger = createLogger({
  format: format.combine(format.timestamp(), format.json()),
  transports: [
    new transports.File({
      filename: path.join(LOGGER_PATH, "error.log"),
      level: "error"
    }),
    new transports.File({
      filename: path.join(LOGGER_PATH, "combined.log")
    })
  ]
});

if (NODE_ENV !== "production") {
  const devConsoleFormat = format.printf(info => {
    return `[${info.timestamp} ${info.level.toUpperCase()}]: ${info.message}`;
  });
  logger.add(
    new transports.Console({
      format: devConsoleFormat
    })
  );
}

module.exports = logger;
