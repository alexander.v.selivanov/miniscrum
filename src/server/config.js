// @ts-check

const path = require("path");

// initialize environment config
require("dotenv").config();

const NODE_ENV = process.env.NODE_ENV || "development";
const DB_FILE_PATH =
  process.env.DB_FILE_PATH || path.join(__dirname, "db", "miniScrum.db");
const WEBSERVER_PORT = process.env.WEBSERVER_PORT || 8080;
const WEBSERVER_PUBLIC_PATH =
  process.env.WEBSERVER_PUBLIC_PATH || path.join(__dirname, "public");
const LOGGER_PATH = process.env.LOGGER_PATH || path.join(__dirname, "log");

module.exports.NODE_ENV = NODE_ENV;
module.exports.DB_FILE_PATH = DB_FILE_PATH;
module.exports.WEBSERVER_PORT = WEBSERVER_PORT;
module.exports.WEBSERVER_PUBLIC_PATH = WEBSERVER_PUBLIC_PATH;
module.exports.LOGGER_PATH = LOGGER_PATH;
