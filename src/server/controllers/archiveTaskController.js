// @ts-check

const logger = require("../logger");
const database = require("../database");

const controller = function archiveTaskController() {
  function controller(req, res) {
    let id = req.body.id;
    database
      .loadTask(id)
      .then(task => {
        if (!task) {
          const error = {
            message: `Task with id ${id} not found`
          };
          res.status(404).send(error);
        } else {
          let name = task.name;
          let sprint = require("../const").ARCHIVE;
          let story = task.story;
          let description = task.description;
          database
            .updateTask(id, name, sprint, story, description)
            .then(countUpdated => {
              if (countUpdated === 0) {
                const error = {
                  message: `Task with id ${id} not found`
                };
                res.status(404).send(error);
              } else {
                res
                  .status(200)
                  .send({ message: `Task with id ${id} archived` });
              }
            })
            .catch(reason => {
              const error = {
                message: `Error archive task (id: ${id})`
              };
              logger.error(`${error.message}. Error: `, reason);
              res.status(500).send(error);
            });
        }
      })
      .catch(reason => {
        const error = {
          message: `Error archive task (id: ${id})`
        };
        logger.error(`${error.message}. Error: ${reason}`);
        res.status(500).send(error);
      });
  }
  return controller;
};

module.exports = controller;
