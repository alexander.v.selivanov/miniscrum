// @ts-check

const logger = require("../logger");
const database = require("../database");

const controller = function deleteTaskController() {
  function controller(req, res) {
    let id = req.body.id;
    database
      .deleteTask(id)
      .then(numRemoved => {
        if (numRemoved === 0) {
          const error = {
            message: `Task with id ${id} not found`
          };
          res.status(404).send(error);
        } else {
          res.status(200).send({ message: `Task with id ${id} removed` });
        }
      })
      .catch(reason => {
        const error = {
          message: `Could not remove task with id: '${id}'`
        };
        logger.error(`${error.message}. Error: `, reason);
        res.status(500).send(error);
      });
  }
  return controller;
};

module.exports = controller;
